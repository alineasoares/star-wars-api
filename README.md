# Star Wars API

> API REST proposta por um desafio técnico da B2W Digital.

Esta API armazena informações de planetas da franquia Star Wars (nome, clima e terreno) em um banco de dados NoSQL. É possivel deletar um planeta pelo seu ID e também consultar pelo mesmo campo, nome ou simplesmente listar todos os planetas. Além disso, é realizado uma consulta a [API pública de Stars Wars](https://swapi.dev/about) para recuperar a quantidade de aparições do planeta em filmes no momento em que o mesmo está sendo inserido.

A API foi construída com JAVA 11, Spring Boot 2, MongoDB e Redis, sendo executada em um container Docker.

O consumo dos serviços depende de autenticação. Informar: usuário `starwars` e senha `sw@api`

## API endpoints

| Método | URL | Descrição |
| ------ | ------ | ------ |
| GET | /v1/planets | Retorna uma lista paginada de planetas. |
| GET | /v1/planets/name/{name} | Retorna um planeta com nome igual a {name} |
| GET | /v1/planets/{id} | Retorna um planeta com id igual a {id} |
| POST | /v1/planets | Salva um planeta. |
| DELETE | /v1/planets/{id} | Delete um planeta onde o id é igual a {id} |


## Pré Requisitos
   - JDK 11
   - Gradle 6.8.3
   - Docker 20.10.2
   - Docker Compose 1.27.4
 
## Como usar?

   #### Clonar o repository
  
   ```
   $ git@gitlab.com:alineasoares/star-wars-api.git
   
   ```

   **Importante**: O docker deve estar rodando, pois os testes de integração utilizam ´testcontainers´.

    
   #### Iniciar a aplicação

   ```
   $ docker-compose up

   ```
  
   #### Documentação Interativa
  
   ```
   https://localhost:8443/swagger-ui.html

   ``` 

   #### Testes

   Na pipeline do projeto existe um job que executa os testes, no entanto também é possível executá-los a partir do comando abaixo:
   
   ```
   $ gradle test
  
   ```
