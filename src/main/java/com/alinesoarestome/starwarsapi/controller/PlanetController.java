package com.alinesoarestome.starwarsapi.controller;

import com.alinesoarestome.starwarsapi.dto.PlanetDTO;
import com.alinesoarestome.starwarsapi.model.Planet;
import com.alinesoarestome.starwarsapi.service.PlanetService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.function.Function;

@Slf4j
@RestController
@RequestMapping("/v1/planets")
@Validated
@Tag(name = "planets", description = "Planet operations")
public class PlanetController {

    private final PlanetService planetService;

    public PlanetController(PlanetService planetService) {
        this.planetService = planetService;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    @Operation(summary = "Save a planet.", tags = {"planets"})
    @ApiResponse(responseCode = "201", description = "Created")
    public void save(@Valid @RequestBody PlanetDTO planetDTO) {
        planetService.save(planetFunction.apply(planetDTO));
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Fetchs lists of planets.", tags = {"planets"})
    @ApiResponse(responseCode = "200", description = "Ok")
    public Page<Planet> list(@RequestParam(required = false) String name,
                             @RequestParam(required = false, defaultValue = "0") int page,
                             @RequestParam(required = false, defaultValue = "10") int size) {

        Pageable paging = PageRequest.of(page, size);

        if (StringUtils.isBlank(name))
            return planetService.listAll(paging);
        else
            return planetService.listAllByName(name, paging);
    }

    @GetMapping(value = "/name/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Fetchs a single planet by name.", tags = {"planets"})
    @ApiResponse(responseCode = "200", description = "Found")
    public Planet getByName(@Valid @PathVariable(required = true) String name) {
        return planetService.getByName(name);
    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Fetchs a single planet by id.", tags = {"planets"})
    @ApiResponse(responseCode = "200", description = "Found")
    public Planet getById(@Valid @PathVariable(required = true) String id) {
        return planetService.getById(id);
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Remove a single planet by id.", tags = {"planets"})
    @ApiResponse(responseCode = "200", description = "Deleted", content = @Content(schema = @Schema(hidden = true)))
    public void remove(@Valid @PathVariable(required = true) String id) {
        planetService.removeById(id);
    }

    private final Function<PlanetDTO, Planet> planetFunction = planetDTO -> Planet.builder()
            .name(planetDTO.getName())
            .climate(planetDTO.getClimate())
            .terrain(planetDTO.getTerrain()).build();
}
