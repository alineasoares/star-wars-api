package com.alinesoarestome.starwarsapi.util;

public final class Constants {

    public static final String NAME_REQUIRED_MSG = "name is required";
    public static final String PLANET_NAME_NOT_FOUND = "planet with name {%s} not found";
    public static final String PLANET_ID_NOT_FOUND = "planet with name {%s} not found";
    public static final String PLANET_ALREADY_EXISTS = "planet with name {%s} already exists";
    public static final String SWAPI_CLIENT_NAME = "swapi-client";
    public static final String PLANET_CACHE_NAME = "PLANET";
    public static final String SWAPI_CACHE_NAME = "SWAPI";

}
