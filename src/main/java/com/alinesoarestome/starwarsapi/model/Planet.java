package com.alinesoarestome.starwarsapi.model;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Data
@EqualsAndHashCode(of = "name")
@Builder
@Document(collection = "planets")
public class Planet implements Serializable {

    @Id
    private String id;
    private String name;
    private String climate;
    private String terrain;
    private Integer films;
}
