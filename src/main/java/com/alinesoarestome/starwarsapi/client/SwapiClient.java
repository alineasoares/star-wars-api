package com.alinesoarestome.starwarsapi.client;

import com.alinesoarestome.starwarsapi.client.schema.SwapiPlanetsResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(url = "${swapi.service.url}", name = "swapi-client")
public interface SwapiClient {

    @GetMapping(value = "/planets/")
    SwapiPlanetsResponse getPlanets(@RequestParam("search") String name);
}
