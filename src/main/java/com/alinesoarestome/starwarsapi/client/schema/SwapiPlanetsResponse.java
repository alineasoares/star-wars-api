package com.alinesoarestome.starwarsapi.client.schema;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SwapiPlanetsResponse implements Serializable {

    private Integer count;
    private String next;
    private String previous;
    private List<SwapiPlanet> results;

}
