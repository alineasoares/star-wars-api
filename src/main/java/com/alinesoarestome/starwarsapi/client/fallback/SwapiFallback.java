package com.alinesoarestome.starwarsapi.client.fallback;

import com.alinesoarestome.starwarsapi.client.SwapiClient;
import com.alinesoarestome.starwarsapi.client.schema.SwapiPlanetsResponse;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SwapiFallback implements SwapiClient {

    private final Throwable cause;

    public SwapiFallback(Throwable cause) {
        this.cause = cause;
    }

    @Override
    public SwapiPlanetsResponse getPlanets(String name) {
        log.error(String.format("Failed to get films to planet {%s}", name));

        return new SwapiPlanetsResponse(-99,"fallback", null, null);
    }
}
