package com.alinesoarestome.starwarsapi.service;

import com.alinesoarestome.starwarsapi.client.SwapiClient;
import com.alinesoarestome.starwarsapi.client.schema.SwapiPlanet;
import com.alinesoarestome.starwarsapi.exception.BusinessException;
import com.alinesoarestome.starwarsapi.exception.ResourceNotFoundException;
import com.alinesoarestome.starwarsapi.model.Planet;
import com.alinesoarestome.starwarsapi.repository.PlanetRepository;
import com.alinesoarestome.starwarsapi.util.Constants;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;

@Slf4j
@Service
public class PlanetService {

    private final PlanetRepository planetRepository;
    private final SwapiClient swapiClient;

    public PlanetService(PlanetRepository planetRepository, SwapiClient swapiClient) {
        this.planetRepository = planetRepository;
        this.swapiClient = swapiClient;
    }

    @CacheEvict(cacheNames = Constants.PLANET_CACHE_NAME, allEntries = true)
    public Planet save(Planet planet) {
        log.debug("Save planet [ {} ]", planet.toString());

        if (StringUtils.isEmpty(planet.getName()))
            throw new BusinessException(Constants.NAME_REQUIRED_MSG);

        planetRepository.findByNameIgnoreCase(planet.getName()).ifPresent(result -> {
            throw new BusinessException(String.format(Constants.PLANET_ALREADY_EXISTS, result.getName()));
        });

        try {
            getPlanetFromSwapi(planet.getName()).ifPresent(result ->  planet.setFilms(result.getFilms().size()));
        } catch (BusinessException e) {
            log.error(e.getMessage());
            planet.setFilms(0);
        }

        return planetRepository.save(planet);
    }

    @Cacheable(cacheNames = Constants.SWAPI_CACHE_NAME, key = "name")
    private Optional<SwapiPlanet> getPlanetFromSwapi(String name) {
        log.debug("Retrieve all planets from swapi [ {} ]", name);

        return Optional.of(swapiClient.getPlanets(name)
                    .getResults()
                    .parallelStream()
                    .findFirst()).orElseThrow(() -> new BusinessException("Planet not found on swapi"));

    }

    @Cacheable(cacheNames = Constants.PLANET_CACHE_NAME, key = "#root.method.name")
    public Page<Planet> listAll(Pageable pageable) {
        log.debug("List planets [ size = {}, page = {} ]", pageable.getPageSize(), pageable.getPageNumber());

        return planetRepository.findAll(pageable);
    }

    @Cacheable(cacheNames = Constants.PLANET_CACHE_NAME, key = "#root.method.name")
    public Page<Planet> listAllByName(String name, Pageable pageable) {
       log.debug("List planets [name = {}, size = {}, page = {} ]", name, pageable.getPageSize(), pageable.getPageNumber());

       return planetRepository.findByNameContainingIgnoreCase(name.strip(), pageable);
    }

    @Cacheable(cacheNames = Constants.PLANET_CACHE_NAME, key="#name")
    public Planet getByName(String name) {
        log.debug("Find a planet [name = {} ]", name);

        return planetRepository.findByNameIgnoreCase(name.strip()).orElseThrow(() -> new ResourceNotFoundException(String.format(Constants.PLANET_NAME_NOT_FOUND, name)));
    }

    @Cacheable(cacheNames = Constants.PLANET_CACHE_NAME, key="#id")
    public Planet getById(String id) {
        log.debug("Find a planet [id = {} ]", id);

        return planetRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException(String.format(Constants.PLANET_ID_NOT_FOUND, id)));
    }

    @CacheEvict(cacheNames = Constants.PLANET_CACHE_NAME, allEntries = true)
    public void removeById(String id) {
        log.debug("Remove a planet [id = {} ]", id);

        if (Objects.nonNull(getById(id))) {
            planetRepository.deleteById(id);
            log.debug("Planet removed");
        }
    }

}
