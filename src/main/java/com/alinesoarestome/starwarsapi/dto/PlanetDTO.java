package com.alinesoarestome.starwarsapi.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PlanetDTO implements Serializable {

    @NotEmpty(message = "Name is required")
    private String name;
    private String climate;
    private String terrain;
}
