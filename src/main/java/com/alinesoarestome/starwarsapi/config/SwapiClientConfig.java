package com.alinesoarestome.starwarsapi.config;

import com.alinesoarestome.starwarsapi.client.SwapiClient;
import com.alinesoarestome.starwarsapi.client.fallback.SwapiFallback;
import com.alinesoarestome.starwarsapi.util.Constants;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import io.github.resilience4j.ratelimiter.RateLimiter;
import io.github.resilience4j.retry.Retry;
import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwapiClientConfig {

    CircuitBreaker circuitBreaker = CircuitBreaker.ofDefaults(Constants.SWAPI_CLIENT_NAME);
    RateLimiter rateLimiter = RateLimiter.ofDefaults(Constants.SWAPI_CLIENT_NAME);
    Retry retry = Retry.ofDefaults(Constants.SWAPI_CLIENT_NAME);

    FeignDecorators decorators = FeignDecorators.builder()
            .withCircuitBreaker(circuitBreaker)
            .withRateLimiter(rateLimiter)
            .withRetry(retry)
            .withFallbackFactory(SwapiFallback::new)
            .build();

    SwapiClient swapiClient = Resilience4jFeign.builder(decorators).contract(new SpringMvcContract())
            .target(SwapiClient.class, "https://swapi.dev/api");

}
