package com.alinesoarestome.starwarsapi.repository;

import com.alinesoarestome.starwarsapi.model.Planet;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PlanetRepository extends MongoRepository<Planet, String> {

    Optional<Planet> findByNameIgnoreCase(String name);

    Page<Planet> findByNameContainingIgnoreCase(String name, Pageable pageable);

}