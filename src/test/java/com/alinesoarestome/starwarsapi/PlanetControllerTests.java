package com.alinesoarestome.starwarsapi;

import com.alinesoarestome.starwarsapi.dto.PlanetDTO;
import com.alinesoarestome.starwarsapi.model.Planet;
import com.alinesoarestome.starwarsapi.repository.PlanetRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@WithMockUser(username = "starwars", password = "sw@api", roles = "USER")
@Testcontainers
public class PlanetControllerTests {

    @Container
    public static GenericContainer<CustomRedisTestContainer> redisContainer = CustomRedisTestContainer.getInstance().withExposedPorts(6379);

    @DynamicPropertySource
    static void redisProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.redis.host", redisContainer::getContainerIpAddress);
        registry.add("spring.redis.port", redisContainer::getFirstMappedPort);
    }

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PlanetRepository planetRepository;

    @Test
    public void shouldReturnAllPlanetsByName() throws Exception {

        List<Planet> planets = givenPlanets();

        Pageable pageable = PageRequest.of(1, planets.size());
        Page<Planet> page = new PageImpl<>(planets, pageable, planets.size());

        when(planetRepository.findByNameContainingIgnoreCase("alder", pageable)).thenReturn(page);

        mockMvc.perform(get("/v1/planets?name=alder&page=1&size=2"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.content[*].name").value(hasItems("Palderaanoi", "Alderaan")));

    }

    @Test
    public void shouldReturnOnePlanetById() throws Exception {
        when(planetRepository.findById(any())).thenReturn(Optional.of(givenPlanet()));

        mockMvc.perform(get("/v1/planets/{id}", "1"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is("1")))
                .andExpect(jsonPath("$.name", is("Naboo")));

    }

    @Test
    public void shouldReturnOnePlanetByName() throws Exception {

        when(planetRepository.findByNameIgnoreCase(anyString())).thenReturn(Optional.of(givenPlanet()));

        mockMvc.perform(get("/v1/planets/name/{name}", "Naboo"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.name", is("Naboo")));

    }

    @Test
    public void shouldRemovePlanet() throws Exception {
        Planet planet = Planet.builder().id("1").build();

        when(planetRepository.findById(any())).thenReturn(Optional.ofNullable(planet));

        mockMvc.perform(delete("/v1/planets/1"))
                .andExpect(status().isOk());


    }

    @Test
    public void shouldSavePlanet() throws Exception {
        when(planetRepository.findByNameIgnoreCase(any())).thenReturn(Optional.empty());
        when(planetRepository.save(any())).thenReturn(givenPlanet());

        mockMvc.perform(post("/v1/planets")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(givenPlanetDTO())))
                .andExpect(status().isCreated());

    }

    private PlanetDTO givenPlanetDTO() {
        return PlanetDTO.builder()
                .name("Naboo")
                .climate("temperate")
                .terrain("grassy hills, swamps, forests, mountains").build();
    }

    private Planet givenPlanet() {
        return Planet.builder()
                .id("1")
                .name("Naboo")
                .climate("temperate")
                .terrain("grassy hills, swamps, forests, mountains")
                .films(2).build();
    }

    private List<Planet> givenPlanets() {
        var firstPlanet = Planet.builder().id("1").name("Alderaan").climate("temperate").terrain("grasslands, mountains").films(8).build();
        var secondPlanet = Planet.builder().id("2").name("Palderaanoi").climate("arid").terrain("grasslands").films(8).build();

        return Arrays.asList(firstPlanet, secondPlanet);
    }
}
