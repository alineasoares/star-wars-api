package com.alinesoarestome.starwarsapi;

import com.alinesoarestome.starwarsapi.client.SwapiClient;
import com.github.tomakehurst.wiremock.WireMockServer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.io.IOException;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static java.nio.charset.Charset.defaultCharset;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.springframework.util.StreamUtils.copyToString;

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@EnableConfigurationProperties
public class SwapiClientTest {

    private static WireMockServer wireMockServer;

    @BeforeEach
    void configureSystemUnderTest() {
        wireMockServer = new WireMockServer(options().dynamicPort());
        wireMockServer.start();
        configureFor(wireMockServer.port());
        System.getProperties().setProperty("swapi.service.url", wireMockServer.baseUrl());
    }

    @AfterEach
    void stopWireMockServer() {
        wireMockServer.stop();
    }

    @Autowired
    private SwapiClient swapiClient;

    @Test
    public void shouldListPlanets() throws IOException {
        wireMockServer.stubFor(get(urlEqualTo("/planets/?search=Tatooine"))
                .willReturn(
                        aResponse()
                            .withStatus(HttpStatus.OK.value())
                            .withHeader("Content-Type", MediaType.APPLICATION_JSON_VALUE)
                            .withBody(copyToString(
                                       SwapiClientTest.class.getClassLoader().getResourceAsStream("payload/get-swapiplanets-response.json"),
                                       defaultCharset()))));

        var response = swapiClient.getPlanets("Tatooine").getResults();

        assertFalse(response.isEmpty());
        assertThat(response, hasSize(1));
    }
}
