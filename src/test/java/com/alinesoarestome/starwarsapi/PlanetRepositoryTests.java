package com.alinesoarestome.starwarsapi;

import com.alinesoarestome.starwarsapi.model.Planet;
import com.alinesoarestome.starwarsapi.repository.PlanetRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@Testcontainers
public class PlanetRepositoryTests {

    @Container
    public static MongoDBContainer mongoDBContainer = CustomMongoTestContainer.getInstance()
            .withExposedPorts(27017);

    @DynamicPropertySource
    static void mongoDbProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.data.mongodb.uri", mongoDBContainer::getReplicaSetUrl);
    }

    @Autowired
    private PlanetRepository planetRepository;

    private Planet givenPlanet(String name) {
        return Planet.builder()
                .name(name)
                .climate("temperate")
                .terrain("cityscape")
                .films(4).build();
    }

    @Test
    public void shouldSavePlanet() {
        var result = planetRepository.save(givenPlanet("Kamino"));

        assertFalse(result.getId().isEmpty());
    }

    @Test
    public void shouldRemovePlanet() {
        var savedPlanet = planetRepository.save(givenPlanet("Coruscant"));
        planetRepository.delete(savedPlanet);

        assertFalse(planetRepository.findById(savedPlanet.getId()).isPresent());
    }

    @Test
    public void shouldListPlanetsByNameIgnoreCase() {
        var firstPlanet = planetRepository.save(givenPlanet("Geonosis"));
        var secondPlanet = planetRepository.save(givenPlanet("Ceonosis"));

        var allPlanets = planetRepository.findByNameContainingIgnoreCase("osis", Pageable.unpaged());

        assertThat(allPlanets.getTotalElements(), is(greaterThan(0L)));
    }

    @Test
    public void shouldListPlanets() {
        var allPlanets = planetRepository.findAll(Pageable.unpaged());

        assertThat(allPlanets.getTotalElements(), is(greaterThan(0L)));
    }

    @Test
    public void shouldFindPlanetByNome() {
        var savedPlanet = planetRepository.save(givenPlanet("Utapau"));

        assertTrue(planetRepository.findByNameIgnoreCase(savedPlanet.getName()).isPresent());
    }

    @Test
    public void shouldFindPlanetById() {
        var savedPlanet = planetRepository.save(givenPlanet("Mustafar"));

        assertTrue(planetRepository.findById(savedPlanet.getId()).isPresent());
    }
}
