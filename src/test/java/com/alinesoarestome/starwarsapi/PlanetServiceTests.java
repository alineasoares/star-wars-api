package com.alinesoarestome.starwarsapi;

import com.alinesoarestome.starwarsapi.client.SwapiClient;
import com.alinesoarestome.starwarsapi.client.schema.SwapiPlanet;
import com.alinesoarestome.starwarsapi.client.schema.SwapiPlanetsResponse;
import com.alinesoarestome.starwarsapi.exception.BusinessException;
import com.alinesoarestome.starwarsapi.exception.ResourceNotFoundException;
import com.alinesoarestome.starwarsapi.model.Planet;
import com.alinesoarestome.starwarsapi.repository.PlanetRepository;
import com.alinesoarestome.starwarsapi.service.PlanetService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class PlanetServiceTests {

    @Mock
    private PlanetRepository planetRepository;

    @Mock
    private SwapiClient swapiClient;

    @InjectMocks
    private PlanetService planetService;


    private Planet givenPlanetWithId(String id, String name) {
        return Planet.builder()
                .id(id)
                .name(name)
                .climate("temperate")
                .terrain("cityscape")
                .films(4).build();
    }

    private Planet givenPlanet(String name) {
        return givenPlanetWithId(null, name);
    }

    private SwapiPlanetsResponse givenSwapiResponse() {
        return SwapiPlanetsResponse.builder()
                .count(1)
                .results(Arrays.asList(
                            SwapiPlanet.builder()
                                    .name("Tattoine")
                                    .films(Arrays.asList("film1","film2")).build()
                            )
                        ).build();
    }

    @Test
    public void shouldSavePlanetAndFilmsCount() {

        when(planetRepository.save(any())).thenReturn(givenPlanet("Tattoine"));
        when(swapiClient.getPlanets(any())).thenReturn(givenSwapiResponse());
        var result = planetService.save(givenPlanet("Tattoine"));

        assertThat(result, notNullValue());
        assertThat(result.getFilms(), is(4));
        assertThat(result.getName(), equalTo("Tattoine"));
    }

    @Test
    public void shouldThrowExceptionOnSave_whenParamIsBlankOrNull() {

        assertThrows(BusinessException.class, () -> planetService.save(givenPlanet("")));
        assertThrows(BusinessException.class, () -> planetService.save(givenPlanet( null)));
    }

    @Test
    public void shouldThrowExceptionOnSave_whenPlanetAlreadyExists() {

        assertThrows(BusinessException.class, () -> {
            when(planetRepository.findByNameIgnoreCase(any())).thenReturn(Optional.of(givenPlanet("Yavin IV")));
            planetService.save(givenPlanet("Yavin IV"));
        });
    }

    @Test
    public void shouldRemovePlanet() {
        var id= UUID.randomUUID().toString();
        Planet planet = Planet.builder().id(id).build();

        when(planetRepository.findById(id)).thenReturn(Optional.of(planet));
        planetService.removeById(id);

        verify(planetRepository, times(1)).deleteById(planet.getId());
    }

    @Test
    public void shouldListPlanetsByNameIgnoreCase() {
        var firstPlanet = givenPlanet("Geonosis");
        var secondPlanet = givenPlanet("Ceonosis");
        var thirdPlanet = givenPlanet("Tatooine");

        Page<Planet> page = new PageImpl<>(Arrays.asList(firstPlanet, secondPlanet));

        when(planetRepository.findByNameContainingIgnoreCase(any(), any())).thenReturn(page);

        var planets = planetService.listAllByName("onosis", PageRequest.of(1, 2));

        assertThat(planets, not(hasItem(thirdPlanet)));
    }

    @Test
    public void shouldListPlanets() {
        var firstPlanet = givenPlanet("Geonosis");
        var secondPlanet = givenPlanet("Ceonosis");
        var thirdPlanet = givenPlanet("Tatooine");

        Page<Planet> page = new PageImpl<>(Arrays.asList(firstPlanet, secondPlanet, thirdPlanet));

        when(planetRepository.findAll(PageRequest.of(1, 3))).thenReturn(page);

        var planets = planetService.listAll(PageRequest.of(1, 3));

        assertThat(planets.getContent(), hasSize(3));
    }

    @Test
    public void shouldFindPlanetByNome() {
        var planet = givenPlanet("Geonosis");

        when(planetRepository.findByNameIgnoreCase(any())).thenReturn(Optional.of(planet));

        var result = planetService.getByName("geonosis");

        assertThat(result.getName(), equalToIgnoringCase(planet.getName()));
    }

    @Test
    public void shouldThrowExceptionOnGetByName_whenPlanetNotExists() {

        assertThrows(ResourceNotFoundException.class, () -> planetService.getByName("-99"));
    }

    @Test
    public void shouldFindPlanetById() {
        var id = UUID.randomUUID().toString();
        var planet = givenPlanetWithId(id, "Mustafar");

        when(planetRepository.findById(any())).thenReturn(Optional.of(planet));

        var result = planetService.getById(id);

        assertThat(result.getId(), equalTo(planet.getId()));
    }

    @Test
    public void shouldThrowExceptionOnGetById_whenPlanetNotExists() {

        assertThrows(ResourceNotFoundException.class, () -> planetService.getById("-99"));
    }


}
