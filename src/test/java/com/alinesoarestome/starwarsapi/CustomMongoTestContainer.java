package com.alinesoarestome.starwarsapi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testcontainers.containers.MongoDBContainer;

public class CustomMongoTestContainer extends MongoDBContainer {
    private static final Logger logger = LoggerFactory.getLogger(CustomMongoTestContainer.class);
    private static final String IMAGE_VERSION = "mongo";
    private static CustomMongoTestContainer container;

    private CustomMongoTestContainer() {
        super(IMAGE_VERSION);
    }
    public static CustomMongoTestContainer getInstance() {
        if (container == null) {
            container = new CustomMongoTestContainer();
        }
        return container;
    }
    @Override
    public void start() {
        super.start();
        logger.debug("MONGODB STARTED");
    }

    @Override
    public void stop() {
        //do nothing, JVM handles shut down
    }
}
