package com.alinesoarestome.starwarsapi;

import org.testcontainers.containers.GenericContainer;

public class CustomRedisTestContainer extends GenericContainer<CustomRedisTestContainer> {
    private static final String IMAGE_VERSION = "redis";
    private static CustomRedisTestContainer container;

    private CustomRedisTestContainer() {
        super(IMAGE_VERSION);
    }
    public static CustomRedisTestContainer getInstance() {
        if (container == null) {
            container = new CustomRedisTestContainer();
        }
        return container;
    }
    @Override
    public void start() {
        super.start();
    }

    @Override
    public void stop() {
        //do nothing, JVM handles shut down
    }
}
