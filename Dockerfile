FROM gradle:6.8.0-jdk11 as builder

WORKDIR /home/starwars/src
COPY --chown=gradle:gradle . /home/starwars/src

RUN gradle clean build -x test

FROM openjdk:11-jre-slim
WORKDIR /home/starwars/api

COPY --from=builder /home/starwars/src/build/libs/*.jar star-wars-api.jar

ENTRYPOINT ["java", "-jar", "star-wars-api.jar"]